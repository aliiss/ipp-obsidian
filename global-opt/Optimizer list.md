## NG
- ngBayesOptim
	- Some kind of problem with pickling state? There is apparently lambda functions as part of the state. Also this just wraps bayes_opt anyway
- Particle swarm
	- Wasn't quite sure if it was even working, not taking advantage of parallelism 
- Parameterized BO
- Differential evolution
- ParameterizedOnePlusOne
- NonObjectOptimizer

## Other
- GPytorch TuRBO
- SQP
- CMA-ES
- Nelder-Mead
- NLOPT
- COBYLA
- Scipy SHGO