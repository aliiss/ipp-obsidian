---

kanban-plugin: board

---

## In progress

- [ ] **Experiments with different optimizers**
	[Optimizer list](obsidian://open?vault=ipp-obsidian&file=global-opt%2FOptimizer%20list)
	- [ ] Nevergrad optimizers
		- [ ] Problems with trying to pickle GP whose state variables include a lambda function?
	- [ ] TuRBO (not in nevergrad)
		- [ ] Modifying (slightly) uberAI gpytorch implementation
		- [ ] Feed back the endpoints rather than the start point in to the GP
	- [ ] Comparison:
- [ ] **Bayesian optimization function in DESC**
	- [x] Register function 
	- [ ] Wrapper
		- [ ] Verify that alignment is correct between bounds and dofs
		- [x] Return formatting
		- [x] Input handling
	- [ ] Actual bayesian optimization function
		- [ ] Stop criteria - success function
	- [x] Kernels
		- [x] SciPy import
	- [ ] Acquisiiton functions
		- [ ] PI
		- [ ] EI


## Complete

- [ ] **DESC Version**
	- [ ] Repeat results in DESC
		- [x] Write optimization loop
		- [ ] Change L_grid etc back down to 6!
- [ ] **Experiments with Bayesian opt in simsopt**
	1. Bound width
		- [ ] See how much global optimization deterioriates with wider boundaries
			- [x] [2, 98], M = N = 2
			- [x] [5, 95], M = N = 2
			- These work fine but only for finitely many iterations—eventually the acquisition function wants to evaluate at a point that VMEC does not converge at
	2. Same bounds, different objective function
- [ ] **Initial runs**
	- [x] Modify target function to be weighted sum of three target functions - hint in `QH_fixed_resolution.py`
	- Ended up kind of weird, maybe redo this sometime?
	- Wrote code to produce VMEC input files
- [ ] **Bayesian opt attempt**
	- [x] Generating bounds for bayesian optimization
		- [x] Generate from randomly perturbed runs
			- [x] Adequate number of samples to get reasonably tight bounds?
- [ ] **Array runs**
	- [x] Array run 128
	- [x] Re-run 16
- [ ] **Tha Crawler**
	- [ ] Rather than finding the target value at each point the acquisition function interrogates, run a DESC optimization starting from that point
		- [ ] Reduces dimensionality from POV of bayesian optimizer
		- [ ] Trivially parallelizable
	- [ ] Great way of exploring optimization space


## Ideas

- [ ] **Autoencoder for dimensionality reduction**
	Finding maximally compact representations for high fidelity VMEC solutions with an autoencoder
- [ ] **Describe LCFS with elongation, triangularity, etc. or splines as a function of the toroidal coordinate**
	- Reduce number of parameters
	- Potentially Yigit knows somthing about this?
- [ ] **Confidence intervals for the bounds**
	- [ ] Is there some minimum number of runs required to get good bounds?
- [ ] **Writing bayesian optimization in simsopt or desc**
	- [ ] Should not be tantalizingly difficult
	- [ ] Would enable the handling of non-convergence scenarios better—check whether convergence with DESC is okay first
	- [ ] Would enable priors of different shapes—something that is monotonically decreasing in m,n perhaps




%% kanban:settings
```
{"kanban-plugin":"board","list-collapse":[false,false,false]}
```
%%