#daily
## To-do: 
- [ ] Read K&K
```text-progress-bar
Kruskal and Kuslrud: 5/22
```
- [x] Finish poster diagram
- [ ] Start on S parameter calculation

## Notes:
- Talked to Alan about possible future directions of project
	- MXMHD - very, very difficult, but would learn a lot doing this
		- Potentially look for a toy problem to do with this?
			- Some sort of method to find islands given an MHD equilibrium?
			  **TODO: learn more about islands and how they are currently computed**

## Calendar:
