---

kanban-plugin: board

---

## To do

- [ ] **Wiser pattern for coil centers**
	- [ ] Simsopt
	- [ ] DESC
- [ ] **Field object for coils in DESC**
	- [ ] Virtual casing to determine required current distribution
	- [ ] Translate virtual casing to dipoles somewho?


## In Progress

- [ ] **Different constraints to try**
	- [x] Fix everything but current
	- [ ] Rotation with centre fixed
	- [ ] See if adding coils is an option 
	- [ ] Vary radii and shape on the surface
	- [ ] Inter-coil force as an objective?
	- [ ]


## Done

- [ ] **DESC Script for planar coil optimization**
	- [x] Created surface coilset
	- [x] Created toroidal coilset
	- [x] Prelim optimization runs
- [ ] **Downloaded cyclone**
	- [x] Successful test run




%% kanban:settings
```
{"kanban-plugin":"board","list-collapse":[false,false,false]}
```
%%