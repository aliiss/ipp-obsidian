---

kanban-plugin: board

---

## To do (in list of priority)

- [ ] **Smart initial bounding**
	- [ideas](obsidian://open?vault=ipp-obsidian&file=weekly-notes%2F2025-W03)
	- Maybe something with the near-axis expansion?
- [ ] **CMA-ES retry**
- [ ] **Genetic algo**
- [ ] **HMC to optimize the whole target function?**
- [ ] **Generate test set**
	- Maybe do low mode number optimization runs on the QUASR equilibria?


## In progress

- [ ] **Gloloc implementation of turbo**
- [ ] **lower ceiling saasbo**
	- Results actually somewhat competitive with TuRBO globally
	- [ ] Check if the lengthscales are confounded again


## Done





%% kanban:settings
```
{"kanban-plugin":"board","list-collapse":[false,false,false]}
```
%%