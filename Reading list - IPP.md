## Papers

```text-progress-bar
VMEC: 7/10
DESC:0/10
Helander Theory of ... : 3/10
Kruskal & Kulsrud: 3/10
Carey and Shasharina: 0/10
Jorit Lion thesis: 0/10
Allan thesis: 2/5
SPEC paper: 0/10
```


### Library:
[Jorit Lion Thesis](https://depositonce.tu-berlin.de/items/2f78cb6b-525c-463e-a10a-4038f564d2fc)
[Kruskal & Kulsrud](https://inis.iaea.org/collection/NCLCollectionStore/_Public/44/100/44100334.pdf?r=1&r=1#page=125)
Boozer (see [[SPEC + Boozer]])
