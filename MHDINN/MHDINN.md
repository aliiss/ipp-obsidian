---

kanban-plugin: board

---

## Done



## In progress

- [ ] **implement easy neural network in mhdinn**
	- [x] Single network -> 3 heads
	- [ ] Convolution?
- [ ] **Deep Equilibrium Model**
	- [x] Model definition
		- [x] Specify model architecture in mlps.py
		- [x] Configuration
	- [ ] Troubleshooting
		- [ ] Check if params are being updated at all
		- [ ] Check if init is being called every single time


## Not started

- [ ] **Simsopt adapter for MHDINN**
	- [ ] DESC also?
- [ ] **Read about new architectures**
	- [ ] Gauge invariant NN
	- [ ] Equilibrium recursive NN
- [ ] **SIREN neural network**
- [ ] **Transfer learning**
	- [ ] Vacuum eq -> finite $\beta$ equilibrium
- [ ] **stronger shaped equilibria**
	- [ ] SQuID
		- [ ] Prune?
	- [ ] Heliotron
- [ ] **Operator learning**
	- [ ] Start with transfer learning
- [ ] **Max accuracy neural net**
	- Rather than trying to learn Fourier modes, use a neural network directly as a representation i.e. X(psi, phi, theta)
	- [ ] Train on quarter field-period grid




%% kanban:settings
```
{"kanban-plugin":"board","list-collapse":[false,false,false]}
```
%%