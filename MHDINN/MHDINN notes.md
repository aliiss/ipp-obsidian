## General structure of `mhdinn`
- `mhdinn` uses hydra to manage configuration and keep the code completely modular. Unfortunately, this obfuscates the program's structure. Hopefully this clears up what actually happens during training with `mhdinn`. 

1. You run `mhdinn` via a command similar to the one below:

```
mhdinn -cd conf +experiment=<my-experiment-name>
```

First, this goes into the configuration folder `conf` and opens the `<my-experiment-name>.yaml` file for your experiment. The `.yaml` file for your experiment should pretty much follow the same format as the ones in the `conf/examples` folder. These all simply overwrite small parts of the default configurations (also included in the `conf`). 

Then, the script `cli.py` runs and the the function `main` instantiates the config. This means that it is cross-checking the contents (i.e. name and type) of the configuration you supplied for your experiment with the configuration "mirror files" located in `mhdinn/config` . Then, one big final config that will later be written to your output folder called `resolved_config` is written. 

Then, from `resolve.config` the actual python program is instantiated in `instantiate(target_cfg, resolved_conf)`. A python dictionary with all the information in the `resolved_conf` is created. 

2.  `instantiate()` steps into `_target_` of the resolved config, located in `interfaces/function_scripts.py` (if you're training something, it will take you to the `train()` function). 

From here on out it's a little more clear what is happening. 

First, the `autodiff` class is instantiated in `fixed_boundary_model`, meaning that the information relevant to the autodiff class (i.e. choosing what actual neural network model etc) is set up. 

Then, 

## Adding your own model 
- First, the actual architecture of the module must be defined as a subclass of `Module` in jax in `mhdinn/mhdinn/models/ad_modules/mlps.py`
- Parameters to be specified during configuration must be included in the class definition and the "mirror file" in `mhdinn/mhdinn/config/module.py`
	- Be sure to add it under `register_configs()` !


