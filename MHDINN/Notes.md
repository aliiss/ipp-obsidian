- The ideal MHD force balance equation is a **nonlinear** PDE
	- Any linear PDE has the same steady state solution (almost?) by definition of linearity, regardless of the initial value. Ideal MHD is no different. 
- Concretely speaking, we can make the phi coordinates equal


### Big questions
- What can we say about the transformation from magnetic to lab-frame coordinates?
	- Must this be a symplectic transformation?
	- 