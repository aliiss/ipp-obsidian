---

kanban-plugin: board

---

## To-do



## In-progress



## Done

- [ ] **Introduction**
	- [x] VMEC introduction
	- [ ] Mention Andrea's contributions
	- [ ] Motivation
- [ ] **Result plots**
	- [x] 314
	- [x] 334
	- [x] d-shape (?)




%% kanban:settings
```
{"kanban-plugin":"board","list-collapse":[false,false,false]}
```
%%