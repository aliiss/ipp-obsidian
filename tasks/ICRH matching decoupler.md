---

kanban-plugin: board

---

## Done

- [ ] **Create callable function for scattering parameters as a function of $f, \mathscr{l}$**
	- [x] Combine all data into a dataframe
	- [x] use `RBFinterpolator` ? Probably any kind of interpolator works
		- [x] 1D interp. over length, or 2D interp of length, freq?
- [ ] Measurements
- [ ] **Adjusting scattering matrix for adapters**
	- [x] Possibly correct treatment:
		- Rotate S11 toward load by 343 mm + 270 mm
		- Rotate S22 toward  load 343 mm 
		- Rotate S12, S21 toward/away from load by 343 +270 +330


## In progress

- [ ] **Comparison to expected model**
	- [x] Generate model of simple single stub matching
		- [x] Model with parallel stub and matched line
	- [ ] Compare non-ideal behavior


## Not started





%% kanban:settings
```
{"kanban-plugin":"board","list-collapse":[false,false,false]}
```
%%