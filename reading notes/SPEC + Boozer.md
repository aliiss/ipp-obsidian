#theory #code
- Divide pressure profile into discrete steps, within each of which the Beltrami equation is satisfied: $$\nabla \times \textbf{B}  = \mu\textbf{B}$$ (not actually sure if $\mu$ is supposed to be magnetic permeability; doesn't really make sense if it is). Derivation from MHD equilibrium: $$ \begin{aligned}
	  \textbf{J} \times \textbf{B} = \frac{1}{\mu_0} (\nabla \times \textbf{B}) \times \textbf{B} &= \nabla p\\ \textup{ but } \nabla p &= 0 \\
	  \implies \nabla \times \textbf{B} \propto \textbf{B}, \textup{ or }\\\nabla \times \textbf{B} &= \lambda \textbf{B}
	\end{aligned}$$
- mapping from Hamiltonian mechanics to magnetic coordinates—read [Boozer](https://journals.aps.org/rmp/abstract/10.1103/RevModPhys.76.1071) for the full details
$$\begin{align}
t \rightarrow \varphi \\
q \rightarrow \theta \\
p \rightarrow \psi \\
H \rightarrow \chi \\
\textup{Hamilton's equations}: \dot{p}^i = - \frac{\partial H}{\partial q^i}, \dot{q}^i = \frac{\partial H}{\partial p^i} \\ \rightarrow \boxed{\frac{d \psi}{d \varphi} =  -\frac{\partial \chi}{\partial \theta} (= 0), \frac{d \theta}{d \varphi} = \frac{\partial \chi}{\partial \psi}( = \iota )}
\end{align} $$ rationale for the correspondences between energy and $\chi$, momentum and $\psi$ : recall that *anything* can be a set of canonical coordinates $q$ or $p$, so long as they satisfy the **commutation relations** 

- nice discussion of symplectic representation of B field $$\begin{align}
  \textbf{B} = \nabla \psi \times \nabla \theta &+ \nabla \varphi \times \nabla \chi, \textup{ with } \\ \textbf{B} &= \nabla \times \textbf{A},\textup{ and most generally, we can write}\\
  \textbf{A} &= \nabla g + \psi \nabla \theta + \chi \nabla \varphi
  \end{align}$$
	- !! why choose $g$ as a gradient? Is that most general? 