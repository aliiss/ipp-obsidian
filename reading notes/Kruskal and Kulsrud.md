 #theory
- Constraining problem:
	- only consider equilibiria with rational surfaces on the edge
	- statistical-ish method? probability distribution for poincare plots 

	- Bake in invariants mathematically into the neural network representation
		- Perhaps some different set of coordinates where they are already conserved? In K&K paper )(see [[Reading list - IPP]]), they mention something with deriving surface quantities from a general form:
			- with $$\nabla p \cdot (\nabla \times \textbf{w}) = 0,$$$$F = \int \textbf{B} \cdot \textbf{w} d\Omega, G = \int \textbf{j} \cdot \textbf{w} d\Omega $$